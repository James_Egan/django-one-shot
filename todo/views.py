from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from todo.models import TodoItem, Todolist

# Create your views here.


class TodoListView(ListView):
    model = Todolist
    template_name = "todo/list.html"
    context_object_name = "todolist"

    # def get_queryset(self):
    #     return ShoppingItems.objects.filter(user=self.request.user)


class TodoDetailView(DetailView):
    model = Todolist
    template_name = "todo/detail.html"
    

class TodoCreateView(CreateView):
    model = Todolist
    template_name = "todo/create.html"
    fields = ["name"]
    success_url = reverse_lazy("list_todos")


class TodoUpdateView(UpdateView):
    model = Todolist
    template_name = "todo/update.html"
    fields = ["name"]
    # success_url = redirect("list_todos", pk=list.items.pk)
    # success_url = reverse_lazy("show_todolist")


    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.id])
      

class TodoDeleteView(DeleteView):
    model = Todolist
    template_name = "todo/delete.html"
    success_url = reverse_lazy("list_todos")


class CreateTodoItem(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class UpdateTodoItem(UpdateView):
    model = TodoItem
    template_name = "items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.id])