from django.contrib import admin
from todo.models import Todolist, TodoItem
# Register your models here.


class TodolistAdmin(admin.ModelAdmin):
    pass


class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(Todolist, TodolistAdmin)
admin.site.register(TodoItem, TodoItemAdmin)

