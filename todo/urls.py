from django.urls import path

from todo.views import (TodoListView, TodoDetailView, TodoCreateView, TodoUpdateView,TodoDeleteView, CreateTodoItem, UpdateTodoItem)

urlpatterns = [
    path("", TodoListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoDetailView.as_view(), name="show_todolist"),
    path("create/", TodoCreateView.as_view(), name="create_todolist"),
    path("todo/<int:pk>/edit", TodoUpdateView.as_view(), name="update_todolist"),
    path("todo/<int:pk>/delete", TodoDeleteView.as_view(), name="delete_todolist"),
    path("items/create/", CreateTodoItem.as_view(), name="create_todoitem"),
    path("todo/items/<int:pk>/edit", UpdateTodoItem.as_view(), name="update_todoitem")
]